﻿namespace Antick.Cqrs.Handlers
{
    public class HandlerFor<TResult> : IHandlerFor<TResult>
    {
        private readonly IHandlerFactory m_Factory;

        public HandlerFor(IHandlerFactory factory)
        {
            m_Factory = factory;
        }

        public TResult With<THandlerContext>(THandlerContext context) where THandlerContext : IHandlerContext
        {
            return m_Factory.Create<THandlerContext, TResult>().Execute(context);
        }

        public TResult With<THandlerContext>() where THandlerContext : IHandlerContext, new()
        {
            return m_Factory.Create<THandlerContext, TResult>().Execute(new THandlerContext());
        }
    }
}
