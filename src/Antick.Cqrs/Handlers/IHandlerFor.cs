﻿namespace Antick.Cqrs.Handlers
{
    public interface IHandlerFor<out T>
    {
        /// <summary>
        ///     Добавить критерии запроса
        /// </summary>
        T With<THandlerContext>(THandlerContext context)
            where THandlerContext : IHandlerContext;

        /// <summary>
        /// Добавить критерий запроса со значением по-умолчанию
        /// </summary>
        T With<THandlerContext>()
            where THandlerContext : IHandlerContext, new();
    }
}
