﻿namespace Antick.Cqrs.Handlers
{
    public interface IHandlerFactory
    {
        /// <summary>
        ///     Создает команду по контексту команды.
        /// </summary>
        IHandler<THandlerContext, TResult> Create<THandlerContext, TResult>()
            where THandlerContext : IHandlerContext;
    }
}
