﻿namespace Antick.Cqrs.Handlers
{
    public interface IHandlerBuilder
    {
        IHandlerFor<TResult> For<TResult>();
    }
}
