﻿namespace Antick.Cqrs.Handlers
{
    public interface IHandler<in THandlerContext, out TResult>
        where THandlerContext : IHandlerContext
    {
        /// <summary>
        /// Исполнить обработчик
        /// </summary>
        TResult Execute(THandlerContext criterion);
    }
}
