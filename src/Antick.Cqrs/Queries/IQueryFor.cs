﻿namespace Antick.Cqrs.Queries
{
    /// <summary>
    ///     Интерфейс для задания критериев запроса
    /// </summary>
    /// <typeparam name="T">Тип возращаемого запросом значения</typeparam>
    public interface IQueryFor<out T>
    {
        /// <summary>
        ///     Добавить критерии запроса
        /// </summary>
        T With<TCriterion>(TCriterion criterion)
            where TCriterion : ICriterion;

        /// <summary>
        /// Добавить критерий запроса со значением по-умолчанию
        /// </summary>
        T With<TCriterion>()
            where TCriterion : ICriterion, new();
    }
}
