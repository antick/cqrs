﻿namespace Antick.Cqrs.Commands
{
    /// <summary>
    ///     Маркерный интерфейс для контекста команды.
    /// </summary>
    public interface ICommandContext
    {
    }
}
