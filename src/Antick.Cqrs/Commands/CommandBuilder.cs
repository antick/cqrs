﻿using System;
using System.Threading.Tasks;
using Castle.Core.Logging;

namespace Antick.Cqrs.Commands
{
    /// <summary>
    ///     Стандартная реализация интерефейса <see cref="ICommandBuilder" />
    /// </summary>
    public class CommandBuilder : ICommandBuilder
    {
        private readonly ICommandFactory _commandFactory;
        private readonly ILogger m_Logger;

        /// <summary>
        ///     Конструктор.
        /// </summary>
        /// <param name="commandFactory"></param>
        public CommandBuilder(ICommandFactory commandFactory, ILogger logger)
        {
            _commandFactory = commandFactory;
            m_Logger = logger;
        }

        public void Execute<TCommandContext>(TCommandContext commandContext)
            where TCommandContext : ICommandContext
        {
            ICommand<TCommandContext> command;
            try
            {
                command = _commandFactory.Create<TCommandContext>();
            }
            catch (Exception ex)
            {
                var message = $"Not found command for {typeof(TCommandContext).FullName}";
                m_Logger.Error(message, ex);
                throw new Exception(message, ex);
            }
            command.Execute(commandContext);
        }

        public void ExecuteAsync<TCommandContext>(TCommandContext commandContext) where TCommandContext : ICommandContext
        {
            Task.Factory.StartNew(() =>
            {
                try
                {
                    ICommand<TCommandContext> command;
                    try
                    {
                        command = _commandFactory.Create<TCommandContext>();
                    }
                    catch (Exception ex)
                    {
                        var message = $"Not found command for {typeof(TCommandContext).FullName}";
                        m_Logger.Error(message, ex);
                        throw new Exception(message,ex);
                    }
                    command.Execute(commandContext);
                }
                catch (Exception ex)
                {
                    m_Logger.Error($"Error in execute {typeof(TCommandContext).FullName}",ex);
                }
            });
        }
    }
}
