﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Antick.Cqrs.Commands;
using Antick.Cqrs.Handlers;
using Antick.Cqrs.Queries;
using Castle.Facilities.TypedFactory;
using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;

namespace Antick.Cqrs
{
    public class CqrsInstaller : IWindsorInstaller
    {
        private readonly FromAssemblyDescriptor m_AssemblyDescriptor;

        public CqrsInstaller(FromAssemblyDescriptor assemblyDescriptor)
        {
            m_AssemblyDescriptor = assemblyDescriptor;
        }

        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            var commands = m_AssemblyDescriptor
                .BasedOn(typeof(ICommand<>))
                .WithService
                .AllInterfaces()
                .LifestyleTransient();

            container.Register(commands,
                Component.For<ICommandBuilder>().ImplementedBy<CommandBuilder>().LifestyleSingleton(),
                Component.For<ICommandFactory>().AsFactory().LifestyleSingleton()
            );

            var queries = m_AssemblyDescriptor
                .BasedOn(typeof(IQuery<,>))
                .WithService
                .AllInterfaces()
                .LifestyleTransient();

            container.Register(queries,
                Component.For<IQueryBuilder>().AsFactory().LifestyleSingleton(),
                Component.For<IQueryFactory>().AsFactory().LifestyleSingleton(),
                Component.For(typeof(IQueryFor<>)).ImplementedBy(typeof(QueryFor<>)).LifestyleSingleton());


            var handlers = m_AssemblyDescriptor
                .BasedOn(typeof(IHandler<,>))
                .WithService
                .AllInterfaces()
                .LifestyleTransient();

            container.Register(handlers,
                Component.For<IHandlerBuilder>().AsFactory().LifestyleSingleton(),
                Component.For<IHandlerFactory>().AsFactory().LifestyleSingleton(),
                Component.For(typeof(IHandlerFor<>)).ImplementedBy(typeof(HandlerFor<>)).LifestyleSingleton());
        }
    }
}
