using System.Reflection;
using System.Runtime.CompilerServices;


[assembly: AssemblyCompany("Antick")]
[assembly: AssemblyProduct("Antick.Cqrs")]
[assembly: AssemblyCopyright("Copyright � Antick 2017")]
[assembly: AssemblyTrademark("Antick.Cqrs")]
[assembly: AssemblyCulture("")]
[assembly: InternalsVisibleTo("DynamicProxyGenAssembly2")]